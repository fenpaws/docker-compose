#!/bin/bash

# checks if old .env and patchfile exists
if [ -f .env ] || [ -f env.patch ] || [ -f custome-compose.yml ]; then
    read -p "exisiting .env/env.patch/custome-compose.yml file found! deleate and generate a new one? [y/n/clean] " existing_env
    if [ $existing_env = "y" ]; then
        rm .env env.patch custome-compose.yml
        echo "old .env/env.patch file deleted!"
        echo ""

    elif [ $existing_env = "c" ] || [ $existing_env = "clean" ]; then
        echo "cleaning old files"
        echo ""
        rm .env env.patch custome-compose.yml
        exit

    else
        exit
    fi
fi

# start configuration
echo Configuration:
echo ==============================================

read -p "database name: " DB_DATABASE
read -p "database user: " DB_USER
read -p "database password: " DB_PASSWORD
read -p "enable traefik: [y/n]: " traefik_enable

if [ $traefik_enable = "y" ]; then
    traefik_enable="True"
    read -p "domain: " traefik_domain
else
    traefik_enable="False"
fi

# generates the .env file for compose
cat > .env <<EOF
"DB_DATABASE="$DB_DATABASE
"DB_USER="$DB_USER
"DB_PASSWORD="$DB_PASSWORD
"DOMAIN="$traefik_domain
"EnableTraefik=$traefik_enable"
EOF

# generates patchfile for use in swarm enviroments
cat > env.patch <<EOF
20a21,34
9,11c9,11
<       MYSQL_DATABASE: \${DB_DATABASE}
<       MYSQL_USER: \${DB_USER}
<       MYSQL_PASSWORD: \${DB_PASSWORD}
---
>       MYSQL_DATABASE: $DB_DATABASE
>       MYSQL_USER: $DB_USER
>       MYSQL_PASSWORD: $DB_PASSWORD
20a21,34
>     deploy:
>       mode: replicated
>       replicas: 1
>       labels:
>         - traefik.enable=$traefik_enable
>         - traefik.http.routers.wordpress.rule=Host("$traefik_domain")
>         - traefik.http.routers.wordpress.entrypoints=websecure
>         - traefik.http.services.wordpress.loadbalancer.server.port=9090
>         - traefik.http.routers.wordpress.tls.certresolver=letsencrypt
> 
>         - traefik.http.routers.wordpress1.rule=Host("$traefik_domain")
>         - traefik.http.routers.wordpress1.entrypoints=web
>         - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
>         - traefik.http.routers.wordpress1.middlewares=redirect-to-https
25,27c39,41
<       WORDPRESS_DB_USER: \${DB_USER}
<       WORDPRESS_DB_PASSWORD: \${DB_PASSWORD}
<       WORDPRESS_DB_NAME: \${DB_DATABASE}
---
>       WORDPRESS_DB_USER: $DB_USER
>       WORDPRESS_DB_PASSWORD: $DB_PASSWORD
>       WORDPRESS_DB_NAME: $DB_DATABASE
EOF

patch docker-compose.yml env.patch -o custome-compose.yml

echo ""
echo "=========="
echo "ALL DONE!"
echo "=========="
echo ""

echo "docker-compose: sudo docker-compose -f docker-compose.yml up -d"
echo "docker swarm: sudo docker stack deploy --compose-file=custome-compose.yml wordpress"