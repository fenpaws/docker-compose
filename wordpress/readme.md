# A simple Wordpress stack

create a ``.env`` fie with the following entries:

```
# MySQL
DB_DATABASE=
DB_USER=
DB_PASSWORD=

# Traefik Config
DOMAIN=wordpress.domain.com
EnableTraefik=True
```

or use the provided generator script to generate a file.

```bash
sudo chmod +x envgen.sh
./envgen.sh
```