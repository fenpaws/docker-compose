#!/bin/bash

# checks if old .env and patchfile exists
if [ -f .env ] || [ -f env.patch ] || [ -f custome-compose.yml ]; then
    read -p "exisiting .env/env.patch/custome-compose.yml file found! deleate and generate a new one? [y/n/clean] " existing_env
    if [ $existing_env = "y" ]; then
        rm .env env.patch custome-compose.yml
        echo "old .env/env.patch file deleted!"
        echo ""

    elif [ $existing_env = "c" ] || [ $existing_env = "clean" ]; then
        echo "cleaning old files"
        echo ""
        rm .env env.patch custome-compose.yml
        exit

    else
        exit
    fi
fi

# start configuration
echo influxdb configuration:
echo ==============================================
read -p "influxdb hostname/ip: " INFLUX_HOST
read -p "influxdb port: " INFLUX_PORT
read -p "influxdb database name: " INFLUX_DBNAME
read -p "influxdb username: " INFLUX_USERNAME
read -p "influxdb user password: " INFLUX_PASSWORD

read -p "your test host (prodServer, homelab): " TAG_HOST
read -p "your reagion (DE/US): " TAG_REGION
read -p "location of your host (datacenter,office)" TAG_LOCATION

read -p "save json files? [y/n]: " SAVE_JSON

if [ $SAVE_JSON = "y" ]; then
    SAVE_JSON="True"
else
    SAVE_JSON="False"
fi

# generates the .env file for compose
cat >.env <<EOF
INFLUX_HOST=$INFLUX_HOST
INFLUX_PORT=$INFLUX_PORT
INFLUX_DBNAME=$INFLUX_DBNAME
INFLUX_USERNAME=$INFLUX_USERNAME
INFLUX_PASSWORD=$INFLUX_PASSWORD

TAG_HOST=$TAG_HOST
TAG_REGION=$TAG_REGION
TAG_LOCATION=$TAG_LOCATION

SAVE_JSON=$SAVE_JSON

EOF

echo ""
echo "=========="
echo "ALL DONE!"
echo "=========="
echo "docker-compose: sudo docker-compose up -d"
