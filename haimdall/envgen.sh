#!/bin/bash

# checks if old .env and patchfile exists
if [ -f .env ] || [ -f env.patch ] || [ -f custome-compose.yml ]; then
    read -p "exisiting .env/env.patch/custome-compose.yml file found! deleate and generate a new one? [y/n/clean] " existing_env
    if [ $existing_env = "y" ]; then
        rm .env env.patch custome-compose.yml
        echo "old .env/env.patch file deleted!"
        echo ""

    elif [ $existing_env = "c" ] || [ $existing_env = "clean" ]; then
        echo "cleaning old files"
        echo ""
        rm .env env.patch custome-compose.yml
        exit

    else
        exit
    fi
fi

# start configuration
echo Configuration:
echo ==============================================

read -p "enable traefik: [y/n]: " traefik_enable

if [ $traefik_enable = "y" ]; then
    traefik_enable="True"
    read -p "domain: " traefik_domain
else
    traefik_enable="False"
fi

# generates patchfile for use in swarm enviroments
cat > env.patch <<EOF
13a14,27
> 		deploy:
> 				mode: replicated
> 				replicas: 1
> 				labels:
> 					- traefik.enable=$traefik_enable
> 					- traefik.http.routers.heimdall.rule=Host("$traefik_domain")
> 					- traefik.http.routers.heimdall.entrypoints=websecure
> 					- traefik.http.services.heimdall.loadbalancer.server.port=9090
> 					- traefik.http.routers.heimdall.tls.certresolver=letsencrypt
> 	
> 					- traefik.http.routers.heimdall1.rule=Host("$traefik_domain")
> 					- traefik.http.routers.heimdall1.entrypoints=web
> 					- traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
> 					- traefik.http.routers.heimdall1.middlewares=redirect-to-https
EOF

patch docker-compose.yml env.patch -o custome-compose.yml

echo ""
echo "=========="
echo "ALL DONE!"
echo "=========="
echo ""

echo "docker-compose: sudo docker-compose -f docker-compose.yml up -d"
echo "docker swarm: sudo docker stack deploy --compose-file=custome-compose.yml wordpress"