# My set of docker-compose/swarm stacks and scripts for my everyday docker deployments

Every folder here has its own readme with instructions on how to use them. I'm happy if you have suggestions on what to improve. Give me some ideas on what to do next!

<hr>

## Projekts

- [Traefik](traefik) - super flexible reverxeproxy // __Swarm Ready__
- [simple-monitoring](simple-monitoring) - Grafana & InfluxDB stack // __Swarm Ready__
- [portainer](portainer) - simple to use Docker and Docker Swarm dashboard // __Swarm Ready__
- [Haimdall](haimdall) - a simple application dashboard // __Swarm Ready__
- [GOsSpeed](gospeed) - a simple tool that collect speedtest results and upload them to influxDB

<hr>

## Requirements for my docker-compose files

all my docker-compose data requires at least two networks.

1. traefik-net

```bash
docker network create --driver overlay traefik-net # for docker swarm
or
docker network create traefik-net # for non swarm docker
```

2. monitor-net

```bash
docker network create --attachable --driver overlay monitor-net # for docker swarm
or
docker network create --attachable monitor-net # for non swarm docker
```

`traefik-net` is an `overlay network` in swarm mode or a `bridge network` in non swarm mode.

- This network is responsible for connecting the traefik proxy to all services that need to be accessed from the outside. e.g. Wordpress, Grafana

`monitor-net` is an `overlay network` in swarm mode or a `bridge network` in non swarm mode.

- Its used to connect more apps and projekt do my monitoring backend.
