#!/bin/sh

# checks if old .patch file exists
if [ -f portainer.patch ]; then
    read -p "exisiting portainer.patch file found! deleate and generate a new one? [y/n] " existing_env
    if [ $existing_env = "y" ]; then
        rm .patch
        echo "old portainer.patch file deleted!"
        echo ""
    else
        exit
    fi
fi

# start configuration
echo patch configuration:
echo ==============================================

read -p "Domain: " domain

cat >portainer.patch <<EOF
25a26
>       - traefik-net
30a32,37
>       labels:
>         - traefik.enable=true
>         - traefik.http.routers.portainer.rule=Host("$domain")
>         - traefik.http.routers.portainer.entrypoints=websecure
>         - traefik.http.services.portainer.loadbalancer.server.port=9000
>         - traefik.http.routers.portainer.tls.certresolver=letsencrypt
31a39,42
>         - traefik.http.routers.portainer1.rule=Host("$domain")
>         - traefik.http.routers.portainer1.entrypoints=web
>         - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
>         - traefik.http.routers.portainer1.middlewares=redirect-to-https
35a47,48
>   traefik-net:
>     external: true
EOF
