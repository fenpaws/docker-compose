#!/bin/sh

# downloads newest version of git portainer
curl -L https://downloads.portainer.io/portainer-agent-stack.yml -o portainer-agent-stack.yml

# checks if .patch file exists
if [ -f portainer.patch ]; then

    # patches the portainer-agent-stack.yml and deploys it
    echo "patch found! apply patches!"
    patch portainer-agent-stack.yml portainer.patch

    echo "Deplying!"
    # startup portainer in swarm
    sudo docker stack deploy --compose-file=portainer-agent-stack.yml portainer
else
    echo "no patch found! deplying!"
    # startup portainer in swarm
    sudo docker stack deploy --compose-file=portainer-agent-stack.yml portainer
fi
