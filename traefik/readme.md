# Traefik 2.0 reverse proxy for docker swarm

## 1. RECONFIGURE

Change the following options:

LetsEncrypt:
If you want to use letsencrypt for your SSL Certificates then replace ``EMAIL`` with your email.
```yml
      - "--certificatesresolvers.letsencrypt.acme.email=EMAIL"
```

Network:
if you choose to rename the network then you also need to change the name here
```yml
      - "--providers.docker.network=NETWORK_NAME"
```

## 2. Starting the Proxy
```sh
sudo docker stack deploy --compose-file=~/docker-compose/traefik/docker-compose.yml proxy
```