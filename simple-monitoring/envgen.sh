#!/bin/bash

# checks if old .env and patchfile exists
if [ -f .env ] || [ -f env.patch ] || [ -f custome-compose.yml ]; then
    read -p "exisiting .env/env.patch/custome-compose.yml file found! deleate and generate a new one? [y/n/clean] " existing_env
    if [ $existing_env = "y" ]; then
        rm .env env.patch custome-compose.yml
        echo "old .env/env.patch file deleted!"
        echo ""

    elif [ $existing_env = "c" ] || [ $existing_env = "clean" ]; then
        echo "cleaning old files"
        echo ""
        rm .env env.patch custome-compose.yml
        exit

    else
        exit
    fi
fi

# start configuration
echo influxdb configuration:
echo ==============================================
read -p "influxdb database name: " INFLUXDB_DB
read -p "influxdb username: " INFLUXDB_USER
read -p "influxdb user password: " INFLUXDB_USER_PASSWORD
read -p "activate influxdb admin? [y/n]: " INFLUXDB_ADMIN_ENABLED

if [ $INFLUXDB_ADMIN_ENABLED = "y" ]; then
    INFLUXDB_ADMIN_ENABLED="True"
    read -p "influxdb admin user? [the username of the admin account]: " INFLUXDB_ADMIN_USER
    read -p "influxdb admin password? [set a secure password!]: " INFLUXDB_ADMIN_PASSWORD
else 
    INFLUXDB_ADMIN_ENABLED="False"
fi

echo ""
echo grafana configuration:
echo ==============================================
read -p "grafana admin password: " GF_SECURITY_ADMIN_PASSWORD

echo ""
echo traefik configuration:
echo ==============================================

read -p "enable traefik: [y/n]: " EnableTraefik
if [ $EnableTraefik = "y" ]; then
    EnableTraefik="True"
    read -p "domain: " DOMAIN
else
    EnableTraefik="False"
fi

# generates the .env file for compose
cat > .env <<EOF
"INFLUXDB_DB"=$INFLUXDB_DB
"INFLUXDB_USER"=$INFLUXDB_USER
"INFLUXDB_USER_PASSWORD"=$INFLUXDB_USER_PASSWORD
"INFLUXDB_ADMIN_ENABLED=$INFLUXDB_ADMIN_ENABLED"
"INFLUXDB_ADMIN_USER"=$INFLUXDB_ADMIN_USER
"INFLUXDB_ADMIN_PASSWORD"=$INFLUXDB_ADMIN_PASSWORD
"GF_SECURITY_ADMIN_PASSWORD"=$GF_SECURITY_ADMIN_PASSWORD
"EnableTraefik"=$EnableTraefik
"DOMAIN"=$DOMAIN
EOF

# generates patchfile for use in swarm enviroments
cat > env.patch <<EOF
8c8
<       - GF_SECURITY_ADMIN_PASSWORD=\${GF_SECURITY_ADMIN_PASSWORD}
---
>       - GF_SECURITY_ADMIN_PASSWORD=$GF_SECURITY_ADMIN_PASSWORD
16a17,37
>     deploy:
>        mode: global
>        labels:
>          - traefik.enable=$EnableTraefik
>          - traefik.http.routers.grafana.rule=Host("$DOMAIN")
>          - traefik.http.routers.grafana.entrypoints=websecure
>          - traefik.http.services.grafana.loadbalancer.server.port=3000
>          - traefik.http.routers.grafana.tls.certresolver=letsencrypt
>  
>          - traefik.http.routers.grafana1.rule=Host("$DOMAIN")
>          - traefik.http.routers.grafana1.entrypoints=web
>          - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
>          - traefik.http.routers.grafana1.middlewares=redirect-to-https
>        placement:
>          constraints:
>            - node.role == manager
>        update_config:
>          parallelism: 1
>          delay: 10s
>        restart_policy:
>          condition: on-failure
25,30c46,51
<       - INFLUXDB_DB=\${INFLUXDB_DB}
<       - INFLUXDB_ADMIN_ENABLED=\${INFLUXDB_ADMIN_ENABLED}
<       - INFLUXDB_ADMIN_USER=\${INFLUXDB_ADMIN_USER}
<       - INFLUXDB_ADMIN_PASSWORD=\${INFLUXDB_ADMIN_PASSWORD}
<       - INFLUXDB_USER=\${INFLUXDB_USER}
<       - INFLUXDB_USER_PASSWORD=\${INFLUXDB_USER_PASSWORD} 
---
>       - INFLUXDB_DB=$INFLUXDB_DB
>       - INFLUXDB_ADMIN_ENABLED=$INFLUXDB_ADMIN_ENABLED
>       - INFLUXDB_ADMIN_USER=$INFLUXDB_ADMIN_USER
>       - INFLUXDB_ADMIN_PASSWORD=$INFLUXDB_ADMIN_PASSWORD
>       - INFLUXDB_USER=$INFLUXDB_USER
>       - INFLUXDB_USER_PASSWORD=$INFLUXDB_USER_PASSWORD 
EOF

patch docker-compose.yml env.patch -o custome-compose.yml

echo ""
echo "=========="
echo "ALL DONE!"
echo "=========="
echo "docker-compose: sudo docker-compose -f custome-compose.yml up -d"
echo "docker swarm: sudo docker stack deploy --compose-file=custome-compose.yml monitoring"