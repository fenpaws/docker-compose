# A simple monitoring/metrics stack with InfluxDB and Grafana

create a ``.env`` fie with the following entries:

```
# influxDB
INFLUXDB_DB=metrics
INFLUXDB_USER=grafana
INFLUXDB_USER_PASSWORD=
INFLUXDB_ADMIN_ENABLED=true
INFLUXDB_ADMIN_USER=admin
INFLUXDB_ADMIN_PASSWORD=

# Grafana admin password
GF_SECURITY_ADMIN_PASSWORD=

# Traefik Config
DOMAIN=grafana.domain.com
EnableTraefik=True
```

or use the provided generator script to generate a file.

```bash
sudo chmod +x envgen.sh
./envgen.sh
```